<?php

namespace App\DataFixtures;

use App\Entity\Actor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ActorFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $a1 = new Actor();

        $a1->setNom("l'éponge")
            ->setPrenom("Bob");

        $manager->persist($a1);

        $a2 = new Actor();

        $a2->setNom("Pouce")
            ->setPrenom("Tom");

        $manager->persist($a2);

        $a3 = new Actor();

        $a3->setNom("Vert")
            ->setPrenom("Le géant");

        $manager->persist($a3);

        $manager->flush();
    }
}
