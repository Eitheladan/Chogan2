<?php

namespace App\DataFixtures;

use App\Entity\Director;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DirectorFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $a1 = new Director();

        $a1->setNom("Halliday")
            ->setPrenom("Johnny");

        $manager->persist($a1);

        $a2 = new Director();

        $a2->setNom("Mouse")
            ->setPrenom("Mikey");

        $manager->persist($a2);

        $a3 = new Director();

        $a3->setNom("Duck")
            ->setPrenom("Donald");

        $manager->persist($a3);

        $manager->flush();
    }
}
